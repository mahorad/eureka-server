# Eureka Server
A Spring Boot application acting as Eureka server for locating services for the purpose of load balancing and failover of middle-tier servers.