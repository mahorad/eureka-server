FROM frolvlad/alpine-oraclejdk8
RUN mkdir -p /app/
ADD /target/eureka-server-0.0.1-SNAPSHOT.jar /app/eureka-server-0.0.1-SNAPSHOT.jar
WORKDIR /app
CMD ["java", "-jar", "eureka-server-0.0.1-SNAPSHOT.jar"]
